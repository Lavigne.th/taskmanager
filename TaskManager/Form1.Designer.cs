﻿namespace TaskManagerWindow
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.listPanel = new System.Windows.Forms.Panel();
            this.listTitleImage = new System.Windows.Forms.PictureBox();
            this.formPanel = new System.Windows.Forms.Panel();
            this.taskAddBtn = new System.Windows.Forms.Button();
            this.taskReturnBtn = new System.Windows.Forms.Button();
            this.formDescriptionTextBox = new System.Windows.Forms.TextBox();
            this.formDescriptionLabel = new System.Windows.Forms.Label();
            this.formMinuteNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.formHLabel = new System.Windows.Forms.Label();
            this.formHourNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.formALabel = new System.Windows.Forms.Label();
            this.formDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.formDeadlineLabel = new System.Windows.Forms.Label();
            this.formTitleTextBox = new System.Windows.Forms.TextBox();
            this.formTitleLabel = new System.Windows.Forms.Label();
            this.listAddTaskBtn = new System.Windows.Forms.Button();
            this.listDataGridView = new System.Windows.Forms.DataGridView();
            this.listPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.listTitleImage)).BeginInit();
            this.formPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.formMinuteNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.formHourNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.listDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // listPanel
            // 
            this.listPanel.Controls.Add(this.listTitleImage);
            this.listPanel.Controls.Add(this.formPanel);
            this.listPanel.Controls.Add(this.listAddTaskBtn);
            this.listPanel.Controls.Add(this.listDataGridView);
            this.listPanel.Location = new System.Drawing.Point(-2, 2);
            this.listPanel.Name = "listPanel";
            this.listPanel.Size = new System.Drawing.Size(800, 671);
            this.listPanel.TabIndex = 0;
            // 
            // listTitleImage
            // 
            this.listTitleImage.Image = ((System.Drawing.Image)(resources.GetObject("listTitleImage.Image")));
            this.listTitleImage.Location = new System.Drawing.Point(112, 11);
            this.listTitleImage.Name = "listTitleImage";
            this.listTitleImage.Size = new System.Drawing.Size(357, 78);
            this.listTitleImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.listTitleImage.TabIndex = 4;
            this.listTitleImage.TabStop = false;
            // 
            // formPanel
            // 
            this.formPanel.Controls.Add(this.taskAddBtn);
            this.formPanel.Controls.Add(this.taskReturnBtn);
            this.formPanel.Controls.Add(this.formDescriptionTextBox);
            this.formPanel.Controls.Add(this.formDescriptionLabel);
            this.formPanel.Controls.Add(this.formMinuteNumericUpDown);
            this.formPanel.Controls.Add(this.formHLabel);
            this.formPanel.Controls.Add(this.formHourNumericUpDown);
            this.formPanel.Controls.Add(this.formALabel);
            this.formPanel.Controls.Add(this.formDateTimePicker);
            this.formPanel.Controls.Add(this.formDeadlineLabel);
            this.formPanel.Controls.Add(this.formTitleTextBox);
            this.formPanel.Controls.Add(this.formTitleLabel);
            this.formPanel.Location = new System.Drawing.Point(0, 0);
            this.formPanel.Name = "formPanel";
            this.formPanel.Size = new System.Drawing.Size(800, 671);
            this.formPanel.TabIndex = 3;
            // 
            // taskAddBtn
            // 
            this.taskAddBtn.Location = new System.Drawing.Point(606, 572);
            this.taskAddBtn.Name = "taskAddBtn";
            this.taskAddBtn.Size = new System.Drawing.Size(146, 48);
            this.taskAddBtn.TabIndex = 11;
            this.taskAddBtn.Text = "ajouter";
            this.taskAddBtn.UseVisualStyleBackColor = true;
            this.taskAddBtn.Click += new System.EventHandler(this.taskAddBtn_Click);
            // 
            // taskReturnBtn
            // 
            this.taskReturnBtn.Location = new System.Drawing.Point(322, 572);
            this.taskReturnBtn.Name = "taskReturnBtn";
            this.taskReturnBtn.Size = new System.Drawing.Size(146, 48);
            this.taskReturnBtn.TabIndex = 10;
            this.taskReturnBtn.Text = "retour";
            this.taskReturnBtn.UseVisualStyleBackColor = true;
            this.taskReturnBtn.Click += new System.EventHandler(this.taskReturnBtn_Click_1);
            // 
            // formDescriptionTextBox
            // 
            this.formDescriptionTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.formDescriptionTextBox.Location = new System.Drawing.Point(322, 295);
            this.formDescriptionTextBox.Multiline = true;
            this.formDescriptionTextBox.Name = "formDescriptionTextBox";
            this.formDescriptionTextBox.Size = new System.Drawing.Size(428, 212);
            this.formDescriptionTextBox.TabIndex = 9;            // 
            // formDescriptionLabel
            // 
            this.formDescriptionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.formDescriptionLabel.Location = new System.Drawing.Point(56, 298);
            this.formDescriptionLabel.Name = "formDescriptionLabel";
            this.formDescriptionLabel.Size = new System.Drawing.Size(194, 45);
            this.formDescriptionLabel.TabIndex = 8;
            this.formDescriptionLabel.Text = "Description";
            // 
            // formMinuteNumericUpDown
            // 
            this.formMinuteNumericUpDown.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.formMinuteNumericUpDown.Location = new System.Drawing.Point(634, 205);
            this.formMinuteNumericUpDown.Maximum = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.formMinuteNumericUpDown.Name = "formMinuteNumericUpDown";
            this.formMinuteNumericUpDown.Size = new System.Drawing.Size(118, 41);
            this.formMinuteNumericUpDown.TabIndex = 7;
            // 
            // formHLabel
            // 
            this.formHLabel.AutoSize = true;
            this.formHLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.formHLabel.Location = new System.Drawing.Point(556, 206);
            this.formHLabel.Name = "formHLabel";
            this.formHLabel.Size = new System.Drawing.Size(32, 36);
            this.formHLabel.TabIndex = 6;
            this.formHLabel.Text = "h";
            // 
            // formHourNumericUpDown
            // 
            this.formHourNumericUpDown.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.formHourNumericUpDown.Location = new System.Drawing.Point(394, 205);
            this.formHourNumericUpDown.Maximum = new decimal(new int[] {
            24,
            0,
            0,
            0});
            this.formHourNumericUpDown.Name = "formHourNumericUpDown";
            this.formHourNumericUpDown.Size = new System.Drawing.Size(118, 41);
            this.formHourNumericUpDown.TabIndex = 5;
            // 
            // formALabel
            // 
            this.formALabel.AutoSize = true;
            this.formALabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.formALabel.Location = new System.Drawing.Point(322, 206);
            this.formALabel.Name = "formALabel";
            this.formALabel.Size = new System.Drawing.Size(31, 36);
            this.formALabel.TabIndex = 4;
            this.formALabel.Text = "à";
            // 
            // formDateTimePicker
            // 
            this.formDateTimePicker.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.formDateTimePicker.Location = new System.Drawing.Point(322, 120);
            this.formDateTimePicker.Name = "formDateTimePicker";
            this.formDateTimePicker.Size = new System.Drawing.Size(428, 41);
            this.formDateTimePicker.TabIndex = 3;
            // 
            // formDeadlineLabel
            // 
            this.formDeadlineLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.formDeadlineLabel.Location = new System.Drawing.Point(56, 117);
            this.formDeadlineLabel.Name = "formDeadlineLabel";
            this.formDeadlineLabel.Size = new System.Drawing.Size(194, 45);
            this.formDeadlineLabel.TabIndex = 2;
            this.formDeadlineLabel.Text = "Deadline";
            // 
            // formTitleTextBox
            // 
            this.formTitleTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.formTitleTextBox.Location = new System.Drawing.Point(322, 38);
            this.formTitleTextBox.Name = "formTitleTextBox";
            this.formTitleTextBox.Size = new System.Drawing.Size(428, 41);
            this.formTitleTextBox.TabIndex = 1;
            // 
            // formTitleLabel
            // 
            this.formTitleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.formTitleLabel.Location = new System.Drawing.Point(56, 42);
            this.formTitleLabel.Name = "formTitleLabel";
            this.formTitleLabel.Size = new System.Drawing.Size(194, 45);
            this.formTitleLabel.TabIndex = 0;
            this.formTitleLabel.Text = "Intitulé";
            // 
            // listAddTaskBtn
            // 
            this.listAddTaskBtn.Location = new System.Drawing.Point(528, 11);
            this.listAddTaskBtn.Name = "listAddTaskBtn";
            this.listAddTaskBtn.Size = new System.Drawing.Size(176, 78);
            this.listAddTaskBtn.TabIndex = 2;
            this.listAddTaskBtn.Text = "ajouter une tâche";
            this.listAddTaskBtn.UseVisualStyleBackColor = true;
            this.listAddTaskBtn.Click += new System.EventHandler(this.listAddTaskBtn_Click);
            // 
            // listDataGridView
            // 
            this.listDataGridView.AllowUserToAddRows = false;
            this.listDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.listDataGridView.Location = new System.Drawing.Point(112, 108);
            this.listDataGridView.Name = "listDataGridView";
            this.listDataGridView.RowTemplate.Height = 28;
            this.listDataGridView.Size = new System.Drawing.Size(591, 565);
            this.listDataGridView.TabIndex = 0;
            this.listDataGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.listDataGridView_CellClick_1);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 674);
            this.Controls.Add(this.listPanel);
            this.Name = "Form1";
            this.Text = "Form1";
            this.listPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.listTitleImage)).EndInit();
            this.formPanel.ResumeLayout(false);
            this.formPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.formMinuteNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.formHourNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.listDataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel listPanel;
        private System.Windows.Forms.DataGridView listDataGridView;
        private System.Windows.Forms.Panel formPanel;
        private System.Windows.Forms.TextBox formTitleTextBox;
        private System.Windows.Forms.Label formTitleLabel;
        private System.Windows.Forms.Button listAddTaskBtn;
        private System.Windows.Forms.Label formDeadlineLabel;
        private System.Windows.Forms.TextBox formDescriptionTextBox;
        private System.Windows.Forms.Label formDescriptionLabel;
        private System.Windows.Forms.NumericUpDown formMinuteNumericUpDown;
        private System.Windows.Forms.Label formHLabel;
        private System.Windows.Forms.NumericUpDown formHourNumericUpDown;
        private System.Windows.Forms.Label formALabel;
        private System.Windows.Forms.DateTimePicker formDateTimePicker;
        private System.Windows.Forms.Button taskReturnBtn;
        private System.Windows.Forms.Button taskAddBtn;
        private System.Windows.Forms.PictureBox listTitleImage;
    }
}

