﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TacheLib;

namespace TaskManagerWindow
{
    public partial class Form1 : Form
    {
        private readonly DataGridViewButtonColumn btn = new DataGridViewButtonColumn();
        private readonly DataGridViewButtonColumn btn2 = new DataGridViewButtonColumn();
        private TaskManager taskManager = new TaskManager("../../save.txt");
        private int indexEditTask;

        public Form1()
        {
            InitializeComponent();
            constructDataGridView();
            //this.Icon = new Icon("res/pen.ico");
            this.Text = "Gestionnaire de tâches";
        }

        private void constructDataGridView()
        {
            formPanel.Hide();

            listDataGridView.ColumnCount = 1;
            listDataGridView.Columns[0].Name = "Titre de la tâche";

            listDataGridView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            addButtonColumn();
            listDataGridView.Columns.Add(btn);
  
            addButtonColumn2();
            listDataGridView.Columns.Add(btn2);
            addRows();
        }


        private void addButtonColumn()
        {
            btn.HeaderText = @"EDITER";
            btn.Name = "button";
            btn.Text = "EDITER";
            btn.UseColumnTextForButtonValue = true;

        }


        private void addButtonColumn2()
        {
            btn2.HeaderText = @"SUPPRIMER";
            btn2.Name = "button2";
            btn2.Text = "SUPPRIMER";
            btn2.UseColumnTextForButtonValue = true;

        }

        private void addRows()
        {
            foreach (PTask task in taskManager.GetTasks())
                listDataGridView.Rows.Add(task.getTitle());

        }

        private void listTaskAddTaskBtn_Click(object sender, EventArgs e)
        {
            listDataGridView.Hide();
            formPanel.Show();
        }

        // ouverture de la fenetre d'édition (affichage edit panel)

        public void editWindow(DataGridViewCellEventArgs e)        
        {
            formPanel.Show();
            taskAddBtn.Text = "modifier";
            listTitleImage.Visible = false;
            fillForm(taskManager.GetTasks()[e.RowIndex]);
            this.indexEditTask = e.RowIndex;
        }

        // suppression d'une tache (liste et fichier)

        public void supprimerTache(DataGridViewCellEventArgs e)        
        {
            taskManager.GetTasks().RemoveAt(e.RowIndex);
            listDataGridView.Rows.Clear();
            addRows();
            taskManager.writeFile("save.txt");
        }

        // gestionnaire des boutons EDIT et SUPPRIMER pour chaque tache dans la liste

        private void listDataGridView_CellClick_1(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 1 && e.RowIndex < taskManager.GetTasks().Count && e.RowIndex >= 0)     // fenetre edition
            {
                editWindow(e);
            }
            else if (e.ColumnIndex == 2 && e.RowIndex < taskManager.GetTasks().Count && e.RowIndex >= 0)    // suppression tache
            {
                DialogResult dr = MessageBox.Show("Supprimer définitivement ?", "Suppression", MessageBoxButtons.YesNo, MessageBoxIcon.Information);

                if (dr == DialogResult.Yes)
                {
                    supprimerTache(e);
                }
            }
        }

        // remplissage des champs du formulaire pour le mode edit

        private void fillForm(PTask task)            
        {
            formTitleTextBox.Text = task.getTitle();
            formDateTimePicker.Value = task.getDateTime();
            formHourNumericUpDown.Value = task.getDateTime().Hour;
            formMinuteNumericUpDown.Value = task.getDateTime().Minute;
            formDescriptionTextBox.Text = task.getDescription();
        }

        // réinitialisation des champs du formulaire

        private void resetForm()      
        {
            formTitleTextBox.Text = "";
            formDateTimePicker.Value = DateTime.Now;
            formHourNumericUpDown.Value = 0;
            formMinuteNumericUpDown.Value = 0;
            formDescriptionTextBox.Text = "";
        }

        // bouton retour pour fenetres ajout et edit
   
        private void taskReturnBtn_Click_1(object sender, EventArgs e)
        {
            listTitleImage.Visible = true;
            listDataGridView.Show();
            formPanel.Hide();
        }

        // récupère la date et l'heure et les concatène
        public DateTime recupererDate()
        {
            DateTime date = formDateTimePicker.Value;
            TimeSpan ts = new TimeSpan((int)formHourNumericUpDown.Value, (int)formMinuteNumericUpDown.Value, 0);
            date = date.Date + ts;
            return date;
        }

        // modification d'une tache (liste et fichier)

        public void modifierTache()
        {
            PTask p = taskManager.GetTasks().ElementAt(indexEditTask);
            p.setTitle(formTitleTextBox.Text);
            DateTime date = recupererDate();
            p.setDateTime(date);
            p.setDescritption(formDescriptionTextBox.Text);
            listDataGridView.Rows.Clear();
            addRows();
            taskManager.writeFile("save.txt");
            confirmationModifSuppr("modification effectuée");
        }

        // ajout d'une tache (liste et fichier)
        public void ajouterTache()
        {
            DateTime date = recupererDate();
            PTask tache = new PTask(taskManager.getCurrentId(), formTitleTextBox.Text, formDescriptionTextBox.Text, date);
            taskManager.addTask(tache, "save.txt");
            listDataGridView.Rows.Add(tache.getTitle());
            confirmationModifSuppr("ajout effectué");
        }



        private void taskAddBtn_Click(object sender, EventArgs e)
        {
            if (taskAddBtn.Text == "modifier")                           //edit_mode
            {
                modifierTache();
            }
            else                                                        // add_mode
            {
                ajouterTache();
            }
        }

        public void confirmationModifSuppr(string message)       // confirme modification et suppression, puis ferme le panel
        {
            MessageBox.Show(message);
            listTitleImage.Visible = false;
            formPanel.Hide();
            resetForm();
        }

        public void listAddTaskBtn_Click(object sender, EventArgs e)   // fenetre ajout tache
        {
            listTitleImage.Visible = false;
            formPanel.Show();
            taskAddBtn.Text = "ajouter";
        }
    }
}
