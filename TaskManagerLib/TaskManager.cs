﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace TacheLib
{
    public class TaskManager
    {
        private List<PTask> tasks = new List<PTask>();
        private int currentId;

        public TaskManager(string path)
        {
            loadTaskList(path);
        }

        /*
         * Charge les taches contenues dans un fichier texte dont le chemin est précisé
         * en paramètre.
         */ 
        private void loadTaskList(string path)
        {
            string[] lines = System.IO.File.ReadAllLines(path);
            currentId = Int32.Parse(lines[0]);

            for (int i = 1; i < lines.Length; i++)
                if (lines[i].Length > 1)
                    tasks.Add(PTask.createPTaskFromString(lines[i]));
            
        }

        /*
         * Ajoute une tache dans la liste, incrémente l'ID pour la tache suivante
         * et écrit la liste de tache dans le fichier texte passé en parametre.
         */ 
        public void addTask(PTask tache, string path)
        {
            tasks.Add(tache);
            setCurrentId(currentId + 1);
            writeFile(path);
        }

        /*
         * Ecrit la liste de tache dans un fichier dont le chemin est passé en paramètre.
         * 
         */ 
        public void writeFile(string path)
        {
            try
            {
                StreamWriter sw = new StreamWriter(path);       // ouverture fichier

                sw.WriteLine(getCurrentId());

                foreach (PTask tache in GetTasks())             // écriture de chaque tache dans le fichier
                {
                    string line = string.Format("{0};{1};{2};{3}", tache.getId(), tache.getTitle(), tache.getDateTime(), tache.getDescription());
                    sw.WriteLine(line);
                }

                sw.Close();                             // fermeture du fichier
            }
            catch (Exception e)
            {
                string errorLine = string.Format("Exception : {0}", e.Message);
                Console.WriteLine(errorLine);
            }
        }

        public int getCurrentId()
        {
            return currentId;
        }

        public void setCurrentId(int id)
        {
            this.currentId = id;
        }

        public List<PTask> GetTasks()
        {
            return this.tasks;
        }
    }
}
