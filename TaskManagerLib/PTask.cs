﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TacheLib
{
    public class PTask
    {

        private string title, description;
        private int id;
        private DateTime deadLine;

        public PTask(int id, string title, string description, DateTime deadLine)
        {
            this.title = title;
            this.description = description;
            this.id = id;
            this.deadLine = deadLine;
        }

        public PTask(string title, string description, DateTime deadLine)
        {
            this.title = title;
            this.description = description;
            this.deadLine = deadLine;
        }


        public string getTitle() { return title; }
        public void setTitle(string name) { this.title = name; }
        public DateTime getDateTime() { return this.deadLine; }
        public void setDateTime(DateTime date) { this.deadLine = date; }
        public string getDescription() { return this.description; }
        public void setDescritption(string description) { this.description = description; }
        public int getId() { return id; }
        public void setId(int id) { this.id = id; }

        /*
         * Créé une PTask à partir d'une chaine de caractères.
         */
        public static PTask createPTaskFromString(string str) {
            String[] taskStr = str.Split(';');
            int id = Int32.Parse(taskStr[0]);
            string title = taskStr[1];
            string descritpion = taskStr[3].Replace("||", "\n");
            DateTime deadline = Convert.ToDateTime(taskStr[2]);
            return new PTask(id, title, descritpion, deadline);
        }
    }
}
