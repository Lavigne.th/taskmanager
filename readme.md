# TaskManager

**Made by Théo & Nico**


[![forthebadge](https://forthebadge.com/images/badges/built-by-developers.svg)](https://forthebadge.com)

[![forthebadge](https://forthebadge.com/images/badges/built-with-love.svg)](https://forthebadge.com)

----------------
Ce projet consiste en un logiciel permettant de gérer des tâches (liste de course, rendez-vous, devoirs à rendre,...).
L'utilisateur peut créer des tâches, les modifier et les supprimer.

----------------

## diagramme UML des classes

![diagramme UML](uml/Classdiagram1.png)


## Etat du projet
**Projet Terminé**  :heavy_check_mark:

##### Améliorations futures possibles :
* tâches ordonnées en fonction de l heure du système et de la deadline
* pouvoir archiver les tâches après la deadline

## License

&copy; Open Source :+1:

## Installation

* **Sous Windows** :poop:

1 ) Télécharger Visual Studio

2 ) Ouvrir le fichier .sln à la racine du projet

## Démarrage

* **Sous Windows** :poop:

1 ) Lancer l exécution du programme

## Développé avec :

* C#


