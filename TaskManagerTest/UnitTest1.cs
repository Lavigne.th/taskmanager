﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TacheLib;
using System.IO;

namespace TaskManagerTest
{
    [TestClass]
    public class PTask_Test
    {
        [TestMethod]
        public void createTaskFromString()
        {
            PTask task = PTask.createPTaskFromString(
                "4;la tache;10/05/2019 12:30:00;C'est une tache.\nUne tache importance!"
               );

            Assert.AreEqual(task.getId(), 4);
            Assert.AreEqual(task.getTitle(), "la tache");
            Assert.AreEqual(task.getDateTime().Year, 2019);
            Assert.AreEqual(task.getDateTime().Month, 5);
            Assert.AreEqual(task.getDateTime().Day, 10);
            Assert.AreEqual(task.getDateTime().Hour, 12);
            Assert.AreEqual(task.getDateTime().Minute, 30);
            Assert.AreEqual(task.getDescription(), "C'est une tache.\nUne tache importance!");
        }

        [TestMethod]
        public void loadTaskList()
        {
            string[] data =
            {
                "6",
                "4;la tache;10/05/2018 12:30:00;C'est une tache.||Une tache importance!",
                "5;la tache;10/05/2018 12:30:00;C'est une tache.||nUne tache importance!",
                "8;la tache;10/05/2018 12:30:00;C'est une tache.||nUne tache importance!"
            };
            File.WriteAllLines("save.txt", data);
            TaskManager manager = new TaskManager("../../save.txt");



            Assert.AreEqual(manager.getCurrentId(), 6);
            Assert.AreEqual(manager.GetTasks()[0].getId(), 4);
            Assert.AreEqual(manager.GetTasks()[1].getId(), 5);
            Assert.AreEqual(manager.GetTasks()[2].getId(), 8);
        }


       
        [TestMethod]
        public void  addTaskTest()
        {
            TaskManager manager = new TaskManager("../../test.txt");
            DateTime date = new DateTime();

            PTask tache1 = new PTask(1,"tache1Test","description tache 1",date);
            PTask tache2 = new PTask(2, "tache2Test", "description tache 2", date);

            manager.addTask(tache1, "../../test.txt");
            manager.addTask(tache2, "../../test.txt");

            Assert.AreEqual(manager.GetTasks()[0].getId(), 1);
            Assert.AreEqual(manager.GetTasks()[0].getTitle(), "tache1Test");
            Assert.AreEqual(manager.GetTasks()[0].getDescription(), "description tache 1");
            Assert.AreEqual(manager.GetTasks()[0].getDateTime(), date);

            Assert.AreEqual(manager.GetTasks()[1].getId(), 2);
            Assert.AreEqual(manager.GetTasks()[1].getTitle(), "tache2Test");
            Assert.AreEqual(manager.GetTasks()[1].getDescription(), "description tache 2");
            Assert.AreEqual(manager.GetTasks()[1].getDateTime(), date);
        }
        
    }
}